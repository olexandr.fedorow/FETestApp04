# [FETestApp04 - Firebase](https://of-cv-f16ed.firebaseapp.com/contacts)
# [FETestApp04 - Netlify](https://of-portfolio-2018.netlify.com/bio)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.2.

`ng new FETestApp4 --skip-test --style=scss --routing --prefix=of`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

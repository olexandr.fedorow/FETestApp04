import {Component, OnInit} from '@angular/core';
import {ConfigurationService} from '@app/services/configuration.service';
import {ExperienceModel} from '@app/models/experience-model';

@Component({
  selector: 'of-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.scss']
})
export class ExperienceComponent implements OnInit {

  public experience: Array<ExperienceModel>;

  constructor(private configService: ConfigurationService) {
  }

  ngOnInit() {
    this.experience = this.configService.configuration.experience;
  }

}
